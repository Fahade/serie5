package org.mssa.exo12;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List <String> strings = new ArrayList<String>();
		strings.add("one");
		strings.add("two");
		strings.add("three");
		strings.add("four");
		strings.add("five");
		strings.add("six");
		strings.add("seven");
		strings.add("eight");
		strings.add("nine");
		strings.add("ten");
		strings.add("eleven");
		strings.add("twelve");
		
		//Contenu de strings avec forEach
		strings.forEach(string -> System.out.println(string));
		
		System.out.println("");
		//Chaines de 3 caract�res dans strings
		List <String> strings3 =new ArrayList <String> ();
		strings3.addAll(strings);
		strings3.removeIf(s->s.length()!=3);
		strings3.forEach(string-> {if (string.length() == 3) System.out.println(string);});
		
		System.out.println("");
		//Liste strings tri�e
		Comparator<String> longueur = (s1,s2) -> s1.length() - s2.length();
	    strings.sort(Comparator.nullsLast(longueur));
		strings.forEach(string -> System.out.println(string));

		System.out.println("");
		//Q4 : Cl� : longueur de chaine, Valeur : liste des chaines qui ont cette longueur
		Map<Integer,HashSet<String>> stringsMap = new HashMap<>(); 
		for(String string:strings) {
			stringsMap.computeIfAbsent(string.length(),hs -> new HashSet<String>()).add(string);
		}
		stringsMap.forEach((k,v)->System.out.println("Longueur de chaine : " + k + " Liste : " + v));
		
		System.out.println("");
		//Q5 : Cl� : 1ere lettre de chaque chaine, Valeur : liste des chaines qui ont cette 1ere lettre
		Map<String,HashSet<String>> stringsMap2 = new HashMap<>(); 
		for(String string:strings) {
			stringsMap2.computeIfAbsent(""+string.charAt(0),hs -> new HashSet<String>()).add(string);
		}
		stringsMap2.forEach((k,v)->System.out.println("Premi�re lettre de chaine : " + k + " Liste : " + v));
		
		System.out.println("");
		//Q6 : Cl� : 1ere lettre de chaque chaine, Valeur : table de hachage Q4
		Map<String,Map<Integer,HashSet<String>>> stringsMap3 = new HashMap<>(); 
		
		
		for(String string:strings) {
			
			stringsMap3.computeIfAbsent(""+string.charAt(0),m -> new HashMap<Integer,HashSet<String>>()).computeIfAbsent(string.length(), hs -> new HashSet<String>()).add(string);

		}
		stringsMap3.forEach((k,v)->System.out.println("Premi�re lettre de chaine : " + k + " Liste : " + v));
		
		System.out.println("");
		//Q7 : Cl� : longueur de chaine, Valeur : chaines de caract�re associ�es s�par�es par virgule
		Map<Integer,String> mapMerge=new HashMap <>();
		for (String string : strings)
		{
		mapMerge.merge(string.length(), string, (s1,s2)->s1+","+s2);
		}
		mapMerge.forEach((k,v)->System.out.println("Longueur de chaine : " + k +" : "+v));

	}

}
